from sqlalchemy import create_engine, Column, String, Integer
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

def makeUser(Base):
  class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key = True)
    username = Column(String, unique = True)
    contact = Column(String, unique = True)
    courses = Column(String)  # course_0:course_1:course_2
    first_name = Column(String)
    last_name = Column(String)
    
    def __repr__(self):
      return "<User(name='%s', fullname='%s', nickname='%s')>" % (
        self.name, self.fullname, self.nickname)
  return User
