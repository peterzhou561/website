from flask_admin.contrib.sqla import ModelView as SqlAModelView
from flask_login import current_user


class SqlAView(SqlAModelView):
  def is_accessible(self):
    return current_user.is_authenticated and 'admin' in current_user.group.split(' ')


class UserView(SqlAView):
  column_exclude_list = ['authenticated', 'hash']


class CourseView(SqlAView):
  pass

class ActionView(SqlAView):
  can_create = False
  can_edit = False
  can_delete = True
  column_list = ('Action UID', 'User UID', 'Action Name', 'Action Status', 'Action Description')
  list_columns = ('action_uid', 'user_uid', 'action_name', 'action_status', 'action_desc')

class IndexView(SqlAView):
  pass
