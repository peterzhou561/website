import pprint
import traceback
import uuid

import sqlalchemy
import werkzeug.security
from flask_login import UserMixin
from sqlalchemy import create_engine, text
from sqlalchemy.orm import sessionmaker, scoped_session
import base64
import os

# def init_db(app = None):
#   engine = create_engine('sqlite:///test.db', echo = True)
#   Base = declarative_base()
#   Base.metadata.create_all(engine)
#   Session = sessionmaker(bind = engine)
#   return engine, Base, Session


class Database():
  def __init__(self, *args, uri = 'splite://test2.db', engine_args = [], engine_kwargs = {}, connect_args = None,
               new_session = None, **kwargs):
    if connect_args is None:
      connect_args = {'timeout': 60}
    if new_session is None:
      self.new_session = scoped_session(sessionmaker(bind = create_engine(
        uri, *engine_args, connect_args = connect_args, **engine_kwargs)))
    else:
      self.new_session = new_session
    # self.new_session = scoped_session(sessionmaker(bind = create_engine(self.uri, *engine_args, connect_args = connect_args, **engine_kwargs)))


class User(object):
  __hash__ = object.__hash__

  def __init__(self, username, user_uuid, group, is_authenticated = False, friendlyname = None, profile = None):
    self.username = username
    if friendlyname == None:
      friendlyname = username
    self.friendlyname = friendlyname  # TODO: Implement soon!
    self.user_uuid = user_uuid.fetchall()[0][0]
    self.group = group
    self.is_authenticated = is_authenticated
    self.is_active = self.is_authenticated
    self.profile = profile

  def get_id(self):
    try:
      return self.username
    except AttributeError:
      raise NotImplementedError('No `id` attribute - override `get_id`')

  def in_group(self, group):
    if group in self.group.split(' '):
      return True
    else:
      return False

  def __eq__(self, other):
    '''
    Checks the equality of two `UserMixin` objects using `get_id`.
    '''
    if isinstance(other, UserMixin):
      return self.get_id() == other.get_id()
    return NotImplemented

  def __ne__(self, other):
    '''
    Checks the inequality of two `UserMixin` objects using `get_id`.
    '''
    equal = self.__eq__(other)
    if equal is NotImplemented:
      return NotImplemented
    return not equal


class UserAdvancedInfosDatabase(Database):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

  def setup(self):
    session = self.new_session()
    print('a')
    session.execute(text(
      'create table if not exists usercourseinfo( uuid text not null, i int not null, courses text not null, '
      'status text not null, marks text not null, primary key(uuid, i));'))
    print('b')
    session.execute(text(
      'create table if not exists userpersonalinfo( uuid text not null, firstname text not null, '
      'lastname text not null, age int not null, notes text not null, primary key(uuid));'))
    print('c')
    session.execute(text(
      'create table if not exists usercontactinfo( uuid text not null, i int not null, firstname text not null, '
      'lastname text not null, relationship not null, contacttype text not null, contact text not null, primary key(uuid, i));'))
    print('d')
    session.commit()

  def close(self):
    pass
    # self.engine.close()

  def new_user(self,
               user_uuid, firstname, lastname, age = -1, notes = '',
               contacts = [], courses = []):
    session = self.new_session()
    session.execute(text(
      'insert into userpersonalinfo (uuid, firstname, lastname, age, notes) '
      'values (:uuid, :fname, :lname, :age, :notes)'),
      {'uuid': user_uuid, 'fname': firstname, 'lname': lastname, 'age': age, 'notes': notes})
    for i in range(len(contacts)):
      contact = contacts[i]
      fname = contact['firstname']
      lname = contact['lastname']
      rela = contact['relationship']
      ctype = contact['contacttype']
      ctct = contact['contact']
      session.execute(text(
        'insert into usercontactinfo (uuid, i, firstname, lastname, relationship, contacttype, contact) '
        'values (:uuid, :i, :fname, :lname, :rela, :ctype, :ctct)'),
        {'uuid': user_uuid, 'i': i, 'fname': fname, 'lname': lname, 'rela': rela, 'ctype': ctype, 'ctct': ctct})
    for i in range(len(courses)):
      course = courses[i]
      name = course['courses']
      status = course['status']
      marks = course['marks']
      session.execute(text(
        'insert into usercontactinfo (uuid, i, courses, status, marks) '
        'values (:uuid, :i, :courses, :status, :marks)'),
        {'uuid': user_uuid, 'i': i, 'courses': name, 'status': status, 'marks': marks})
    return 0, user_uuid

  def existing_user(self, user_uuid):
    session = self.new_session()
    result = session.execute("select exists(select * from userpersonalinfo WHERE uuid=:user_uuid);",
                             {'user_uuid': user_uuid})
    return bool(result.fetchall()[0])

  def delete_user(self, user_uuid):
    session = self.new_session()
    session.execute(text("delete from users where (uuid = :user_uuid);"), {'user_uuid': user_uuid})
    session.commit()

  def edit_user(self, user_uuid, firstname, lastname, age = -1, notes = '',
                contacts = [], courses = []):
    if not self.existing_user(user_uuid):
      return 1, user_uuid
    if user_uuid is None:
      return 1, user_uuid
    if firstname is None:
      firstname = self.get_user_info(user_uuid, personal = True)[2]['personal']['firstname']
    if lastname is None:
      lastname = self.get_user_info(user_uuid, personal = True)[2]['personal']['lastname']
    if age is None:
      age = self.get_user_info(user_uuid, personal = True)[2]['personal']['age']
    if notes is None:
      notes = self.get_user_info(user_uuid, personal = True)[2]['personal']['notes']
    if courses is None:
      courses = self.get_user_info(user_uuid, course = True)[2]['courses']
    if contacts is None:
      contacts = self.get_user_info(user_uuid, contact = True)[2]['contact']
    self.delete_user(user_uuid)
    self.new_user(user_uuid, firstname, lastname, age, notes, contacts, courses)
    return 0, user_uuid

  def list_user_uuids(self):
    session = self.new_session()
    result = session.execute(text("select uuid from userpersonalinfo;")).fetchall()
    user_uuids = []
    for i in result:
      user_uuids.append(i[0])
    return user_uuids

  def list_users(self):
    users = []
    users_uuid = {}
    session = self.new_session()
    user_personals = session.execute(text("select * from userpersonalinfo;")).fetchall()
    user_courses = session.execute(text("select * from usercourseinfo;")).fetchall()
    user_contacts = session.execute(text("select * from usercontactinfo;")).fetchall()
    for user_personal in user_personals:
      users.append({
        'user_uuid': user_personal[0],
        'firstname': user_personal[1],
        'lastname': user_personal[2],
        'age': user_personal[3],
        'notes': user_personal[4],
        'courses': [],
        'contacts': [],
      })
      users_uuid[user_personal[0]] = len(users) - 1
    # for user_contact in user_contacts:
    #     user_uuid = user_contact[0]
    #     i = user_contact[1]
    #     firstname = user_contact[2]
    #     lastname = user_contact[3]
    #     relationship = user_contact[4]
    #     contacttype = user_contact[5]
    #     contact = user_contact[6]
    #     while len(users[users_uuid[user_uuid]]['contacts']) - 1 < i:
    #         users[users_uuid[user_uuid]]['contacts'].append({})
    #     users[users_uuid[user_uuid]]['contacts'][i] = {
    #         'firstname': firstname,
    #         'lastname': lastname,
    #         'relationship': relationship,
    #         'contacttype': contacttype,
    #         'contact': contact,
    #     }
    for user_course in user_courses:
      user_uuid = user_course[0]
      i = user_course[1]
      course = user_course[2]
      status = user_course[3]
      marks = user_course[4]
      for k in users:
        if k['user_uuid'] == user_uuid:
          while len(k['courses']) - 1 < i:
            k['courses'].append({})
            k['courses'][i] = {
              'courses': course,
              'status': status,
              'marks': marks,
            }

    return users, 0

  def get_user_info(self, user_uuid, personal = True, course = True, contact = True):
    '''
    Gets user info in a dictionary.
    :param user_uuid: User UUID.
    :param personal: Get Personal data?
    :param course: Get Course data?
    :param contact: Get Contact data?
    :return: list with exit code as first and user UUID as second.
    '''
    if self.existing_user(user_uuid):
      session = self.new_session()
      info = {}
      if personal:
        info['personal'] = {}
        info['personal']['firstname'] = session.execute(
          text("select firstname from userpersonalinfo where (uuid = :user_uuid)"),
          {'user_uuid': user_uuid}).fetchall()[0][0]
        info['personal']['lastname'] = session.execute(
          text("select lastname from userpersonalinfo where (uuid = :user_uuid)"),
          {'user_uuid': user_uuid}).fetchall()[0][0]
        info['personal']['age'] = \
          session.execute(text("select age from userpersonalinfo where (uuid = :user_uuid)"),
                          {'user_uuid': user_uuid}).fetchall()[0][0]
        info['personal']['notes'] = session.execute(
          text("select notes from userpersonalinfo where (uuid = :user_uuid)"),
          {'user_uuid': user_uuid}).fetchall()[0][0]
      if course:
        info['courses'] = []
        courses_len = session.execute(
          text("select * from usercourseinfo where (uuid = :user_uuid)"),
          {'user_uuid': user_uuid}).fetchall()
        for i in range(len(courses_len)):
          info['courses'].append({})
          info['courses'][i]['courses'] = session.execute(
            text("select courses from usercourseinfo where uuid = :user_uuid and i = :i"),
            {'user_uuid': user_uuid, 'i': i}).fetchall()
          info['courses'][i]['status'] = session.execute(
            text("select status from usercourseinfo where uuid = :user_uuid and i = :i"),
            {'user_uuid': user_uuid, 'i': i}).fetchall()
          info['courses'][i]['marks'] = session.execute(
            text("select marks from usercourseinfo where uuid = :user_uuid and i = :i"),
            {'user_uuid': user_uuid, 'i': i}).fetchall()
          info['courses'][i]['level'] = session.execute(
              text("select lvl from usercourseinfo where uuid = :user_uuid and i = :i"),
              {'user_uuid': user_uuid, 'i': i}).fetchall()
      if contact:
        info['contact'] = []
        contacts_len = session.execute(
          text("select * from usercontactinfo where (uuid = :user_uuid)"),
          {'user_uuid': user_uuid}).fetchall()
        for i in range(len(contacts_len)):
          info['contact'].append({})
          info['contact'][i]['contact'] = session.execute(
            text("select contact from usercontactinfo where (uuid = :user_uuid, i = ;i)"),
            {'user_uuid': user_uuid, 'i': i})
          info['contact'][i]['contacttype'] = session.execute(
            text("select contacttype from usercontactinfo where (uuid = :user_uuid, i = ;i)"),
            {'user_uuid': user_uuid, 'i': i})
          info['contact'][i]['relationship'] = session.execute(
            text("select relationship from usercontactinfo where (uuid = :user_uuid, i = ;i)"),
            {'user_uuid': user_uuid, 'i': i})
          info['contact'][i]['firstname'] = session.execute(
            text("select firstname from usercontactinfo where (uuid = :user_uuid, i = ;i)"),
            {'user_uuid': user_uuid, 'i': i})
          info['contact'][i]['lastname'] = session.execute(
            text("select lastname from usercontactinfo where (uuid = :user_uuid, i = ;i)"),
            {'user_uuid': user_uuid, 'i': i})
      return 0, user_uuid, info
    else:
      return 1, user_uuid, None


class UserBasicInfosDatabase(Database):
  def __init__(self,
               *args,
               method = 'pbkdf2:sha256:100000',
               salt_length = 512,
               **kwargs):
    super().__init__(*args, **kwargs)
    self.authentication_status = {}
    self.method = method
    self.salt_length = salt_length
  
  def setup(self):
    session = self.new_session()
    session.execute(text(
      'create table if not exists users( username text not null, friendlyname text not null, '
      'passwordHash text not null, uuid text not null, groupName text not null, primary key(uuid), unique(username));'))
    session.commit()
  
  def close(self):
    pass
    # self.engine.close()
  
  def gen_hash(self, password) -> bool:
    result = werkzeug.security.generate_password_hash(
      password,
      method = self.method,
      salt_length = self.salt_length)
    print('gen_hash', password, result)
    return result
  
  def check_hash(self, password_hash, password) -> bool:
    result = werkzeug.security.check_password_hash(password_hash, password)
    print('check_hash', password_hash, password, result)
    return result
  
  def new_user(self, username, password, user_uuid = str(uuid.uuid4()), group = ''):
    # password_hash = werkzeug.security.generate_password_hash(
    #   password,
    #   method = self.method,
    #   salt_length = self.salt_length)
    password_hash = self.gen_hash(password)
    session = self.new_session()
    try:
      session.execute(
        text(
          "insert into users (username, passwordHash, uuid, groupName) values (:username, :password_hash, :user_uuid, :group_name);"),
        {'username': username, 'password_hash': password_hash, 'user_uuid': user_uuid, 'group_name': group})
    except sqlalchemy.exc.IntegrityError as err:
      if 'unique constraint' in str(err.orig).lower():
        return 1, None, None, None
      else:
        print(traceback.format_exc())
        print('unknown integrity error')
        return 2, None, None, None
    session.commit()
    return 0, username, password_hash, user_uuid
  
  def existing_user(self, username = None, user_uuid = None):
    assert username is not None or user_uuid is not None
    session = self.new_session()
    if username is not None:
      result = session.execute("select exists(select * from users WHERE username=:username);",
                               {'username': username})
    else:
      result = session.execute("select exists(select * from users WHERE uuid=:user_uuid);",
                               {'user_uuid': user_uuid})
    return bool(result.fetchall()[0])
  
  def get_user(self, user_id, is_authenticated = None, profile = None):
    if is_authenticated is None:
      print('c')
      is_authenticated = self.authentication_status.get(user_id, False)
      print('d', is_authenticated)
    if user_id in self.list_usernames():
      return User(username = user_id, user_uuid = self.get_user_uuid(user_id), group = self.get_group(user_id),
                  is_authenticated = is_authenticated, profile = profile)
    else:
      return None
  
  def check_user(self, username = None, user_uuid = None, password_text = None):
    # # TODO: SECURITY FLAW: FOR DEV ONLY v
    # self.authentication_status[username] = True
    # return True  # TODO: For dev purposes only
    # # TODO: SECURITY FLAW: FOR DEV ONLY ^
    assert password_text is not None
    assert username is not None or user_uuid is not None
    session = self.new_session()
    if username is not None:
      password_hash_ = session.execute(text("select passwordHash from users where (username = :username)"),
                                       {'username': username})
    else:
      password_hash_ = session.execute(text("select passwordHash from users where (uuid = :user_uuid)"),
                                       {'user_uuid': user_uuid})
    password_hash = password_hash_.fetchall()[0][0]
    if not password_hash:
      return False
    else:
      password_ok = self.check_hash(password_hash, password_text)
      if password_ok:
        self.authentication_status[username] = True
      return password_ok
  
  def delete_user(self, username = None, user_uuid = None):
    assert username is not None or user_uuid is not None
    session = self.new_session()
    if username is not None:
      session.execute(text("delete from users where (username = :username);"), {'username': username})
    else:
      session.execute(text("delete from users where (uuid = :user_uuid);"), {'user_uuid': user_uuid})
    session.commit()
  
  def edit_user(self, username = None, new_username = None, user_uuid = None, new_password = None,
                new_group = ''):
    assert username is not None or user_uuid is not None
    assert new_username is not None or new_password is not None
    if self.existing_user(username = username, user_uuid = user_uuid):
      session = self.new_session()
      if new_username is not None:
        if username is not None:
          session.execute(text(
            'update users set username = :new_username where username = :username'
          ), {'new_username': new_username, 'username': username})
        elif user_uuid is not None:
          session.execute(text(
            'update users set username = :new_username where uuid = :uuid'
          ), {'new_username': new_username, 'uuid': user_uuid})
      if new_password is not None:
        if username is not None:
          session.execute(text(
            'update users set passwordHash = :new_password where username = :username'
          ), {'new_password': self.gen_hash(new_password), 'username': username})
        elif user_uuid is not None:
          session.execute(text(
            'update users set passwordHash = :new_password where uuid = :uuid'
          ), {'new_password': self.gen_hash(new_password), 'uuid': user_uuid})
      if new_group is not None:
        if username is not None:
          session.execute(text(
            'update users set groupName = :group_name where username = :username'
          ), {'group_name': new_group, 'username': username})
        elif user_uuid is not None:
          session.execute(text(
            'update users set groupName = :group_name where uuid = :uuid'
          ), {'group_name': new_group, 'uuid': user_uuid})
      return True
    else:
      return False
  
  def get_user_uuid(self, username):
    if username in self.list_usernames():
      session = self.new_session()
      result = session.execute(text("select uuid from users where (username = :username);"),
                               {'username': username})
      return result
    else:
      return None
  
  def get_group(self, username):
    if username in self.list_usernames():
      session = self.new_session()
      result = session.execute(text("select groupName from users where (username = :username);"),
                               {'username': username}).fetchall()[0][0].split(' ')
      return result
    else:
      return None
  
  def list_usernames(self):
    session = self.new_session()
    result = session.execute(text("select username from users;")).fetchall()
    usernames = []
    for i in result:
      usernames.append(i[0])
    return usernames
  
  def list_users(self):
    session = self.new_session()
    result = session.execute(text("select * from users;")).fetchall()
    print('list_users', result)
    return result


class UsersDatabase:
  def __init__(self, *args, uri = 'sqlite:///test_alpha.db',
               engine_args = [], engine_kwargs = {}, connect_args = [], **kwargs):
    self.new_session = scoped_session(sessionmaker(bind = create_engine(
      uri, *engine_args, connect_args = connect_args, **engine_kwargs)))
    self.ubi_db = UserBasicInfosDatabase(*args, new_session = self.new_session, **kwargs)
    self.uai_db = UserAdvancedInfosDatabase(*args, new_session = self.new_session, **kwargs)
    self.admin = AdminInterface(self)
  
  def setup(self):
    self.ubi_db.setup()
    self.uai_db.setup()
  
  def close(self):
    self.ubi_db.close()
    self.uai_db.close()
  
  def get_user(self, *args, **kwargs):
    return self.ubi_db.get_user(*args, **kwargs)
  
  def check_user(self, *args, **kwargs):
    return self.ubi_db.check_user(*args, **kwargs)
  
  def existing_user(self, *args, **kwargs):
    return self.ubi_db.existing_user(*args, **kwargs)
  
  @property
  def authentication_status(self):
    return self.ubi_db.authentication_status
  
  def new_user(self,
               username = None, password = None, user_uuid = str(uuid.uuid4()), group = None,
               firstname = None, lastname = None, age = -1, notes = '',
               contacts = [], courses = []):
    self.ubi_db.new_user(username, password, user_uuid, group)
    self.uai_db.new_user(user_uuid, firstname, lastname, age, notes, contacts, courses)
    return True
  
  def edit_user(self,
                username = None, new_username = None, user_uuid = None, new_password = None, group = None,
                firstname = None, lastname = None, age = -1, notes = '',
                contacts = [], courses = []):
    if user_uuid is None or username is None and user_uuid is None:
      raise RuntimeError('`username` param must not be None, got None.')
    return [
      self.ubi_db.edit_user(username, new_username, user_uuid, user_uuid, new_password, group),
      self.uai_db.edit_user(user_uuid, firstname, lastname, age, notes, contacts, courses), ]
  
  def list_usernames(self):
    return self.ubi_db.list_usernames()
  
  def list_users(self):
    ubi_users = self.ubi_db.list_users()
    uai_users, uai_code = self.uai_db.list_users()
    u_users = []
    for i in range(len(ubi_users)):
      if len(uai_users) - 1 < i:  # doesn't exist in uai_users
        u_users = {'basic': list(ubi_users[i])}
      else:
        u_users = {**{'basic': list(ubi_users[i])}, **uai_users[i]}
    if uai_code != 0:
      code = uai_code
    else:
      code = 0
    return u_users, code
  
  def get_user_profile(self, username):
    user_uuid = self.ubi_db.get_user(user_id = username).user_uuid
    return self.uai_db.get_user_info(user_uuid = user_uuid, personal = True, course = True, contact = True)


class AdminInterface:
  def __init__(self, users_db: UsersDatabase):
    self.users_db = users_db
  
  def new_user(self,
               username = None, password = None, user_uuid = str(uuid.uuid4()), group = None,
               firstname = None, lastname = None, age = -1, notes = '',
               contacts = [], courses = []):
    return self.users_db.new_user(
      username = username, password = password, user_uuid = user_uuid, group = group,
      firstname = firstname, lastname = lastname, age = age, notes = notes,
      contacts = contacts, courses = courses)
  
  def edit_user(self,
                username = None, new_username = None, user_uuid = None, new_password = None, group = None,
                firstname = None, lastname = None, age = -1, notes = '',
                contacts = [], courses = []):
    return self.users_db.edit_user(
      username = None, new_username = None, user_uuid = None, new_password = None, group = None,
      firstname = None, lastname = None, age = -1, notes = '',
      contacts = [], courses = [])
  
  def list_users(self):
    return self.users_db.list_users()
  
  def get_user_profile(self, username):
    return self.users_db.get_user_profile(username)


class CoursesAdvancedInfosDatabase:
  def __init__(self, *args, new_session, **kwargs):
    self.new_session=new_session
    self.users_db = UsersDatabase()
    super().__init__(*args, **kwargs)

  def setup(self):
    session = self.new_session()
    self.users_db.setup()
    print('aa')
    session.execute(text(
      'create table if not exists courses( uuid text not null, name text not null, desc text not null, notes text not null,'
      'primary key(uuid));'))
    session.commit()

  def existing_course(self, course_uuid):
    session = self.new_session()
    result = session.execute("select exists(select * from courses WHERE uuid=:course_uuid);",
                             {'course_uuid': course_uuid})
    return bool(result.fetchall()[0])

  def list_courses(self):
    courses = []
    session = self.new_session()
    allCourses = session.execute(text("select * from courses;")).fetchall()[0]
    print(allCourses)
    for course in allCourses:
      courses.append({
        'uuid': allCourses[0],
        'name': allCourses[1],
        'desc': allCourses[2],
        'notes': allCourses[3],
      })
    return courses

  def get_course_by_uuid(self, course_uuid, lvl):
    if self.existing_course(course_uuid):
      print("getcoursebyuuid")
      session = self.new_session()
      info= {'overview': {}}
      info['overview']['name']=session.execute(
                    text("select name from courses where uuid = :course_uuid and lvl = :lvl"), {'course_uuid': course_uuid, "lvl": lvl}).fetchall()[0][0]
      info['overview']['desc'] = session.execute(
        text("select desc from courses where uuid = :course_uuid and lvl = :lvl"), {'course_uuid': course_uuid, "lvl": lvl}).fetchall()[0][0]
      info['overview']['notes'] = session.execute(
        text("select notes from courses where uuid = :course_uuid and lvl = :lvl"), {'course_uuid': course_uuid, "lvl": lvl}).fetchall()[0][0]
      info['overview']['level'] = session.execute(
          text("select lvl from courses where uuid = :course_uuid and lvl = :lvl"), {'course_uuid': course_uuid, "lvl": lvl}).fetchall()[0][0]
      return info

  def get_course_materials(self, course_uuid, level):
    print("get_course_materials")
    session = self.new_session()
    res = []
    coursedata = session.execute(text("select data from coursematerialinfo where uuid = :course_uuid and lvl = :lvl")
                                         , {'course_uuid': course_uuid, "lvl": level}).fetchall()
    print(coursedata)
    coursetype = session.execute(text("select type from coursematerialinfo where uuid = :course_uuid and lvl = :lvl")
                                 , {'course_uuid': course_uuid, "lvl": level}).fetchall()
    for i in range(len(coursedata)):
      res.append([coursedata[i][0], coursetype[i][0]])
    return res

  def make_material(self, res: list):
    print("advancedcoursesdatabase make_material")
    urls = []
    for i in range(len(res)):
      if res[i][1] == 'question':
        f = open('../temp/' + str(i) + '.txt', "wb")
        f.write(res[i][1])
        continue
      img = base64.b64decode(res[i][0])
      material_uuid = uuid.uuid4()
      file_path = os.path.join('./static/temp', f'{material_uuid}.{res[i][1]}')
      file_url = f'/static/temp/{material_uuid}.{res[i][1]}'
      urls.append(file_url)
      with open(file_path, 'wb') as file:
        file.write(img)
    return urls

  def check_user(self, username, course_uuid, level):
    user = self.users_db.get_user_profile(username)
    print("advancedcorsedatabase check_user")
    print(user)
    for i in user[2]['courses']:
      if int(i['level'][0][0]) != level:
        print("continue")
        continue
      k = i['courses'][0][0]
      if k == course_uuid:
        print("true")
        return True
    return False


class CoursesDatabase(Database):
  def __init__(self, *args, uri = 'sqlite:///test2.db', engine_args = [], engine_kwargs = {}, connect_args = [],
               **kwargs):
    self.new_session = scoped_session(sessionmaker(bind = create_engine(
      uri, *engine_args, connect_args = connect_args, **engine_kwargs)))
    self.cai_db = CoursesAdvancedInfosDatabase(*args, new_session = self.new_session, **kwargs)

  def setup(self):
    self.cai_db.setup()

  def list_courses(self):
    return self.cai_db.list_courses()

  def get_course(self, course_uuid, lvl):
    return self.cai_db.get_course_by_uuid(course_uuid, lvl)

  def check_user(self, username, course_uuid, level):
    return self.cai_db.check_user(username, course_uuid, level)

  def get_course_materials(self, course_uuid, lvl):
    return self.cai_db.get_course_materials(course_uuid, lvl)

  def generate_materials(self, ret):
    return self.cai_db.make_material(ret)


if __name__ == '__main__':
  u_db = UsersDatabase()
  u_db.setup()
  pprint.pprint(u_db.list_users(), indent = 2)

# class User(UserMixin):
#     def __init__(self, id_):
#         self.id = id_
#
#     def is_active(self):
