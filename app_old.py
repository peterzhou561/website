import os
import secrets

import jinja2
from flask import Flask, render_template, abort, request, redirect, url_for, g, jsonify, session, flash
from flask_login import LoginManager, login_required, current_user, logout_user, login_user
from flask_session import Session
import database_old
import json

app_name = 'RoboEDU Instructions'
app = Flask(app_name)
app.config['FLASK_ADMIN_SWATCH'] = 'paper'
app.config['SECRET_KEY'] = b'\xc3\x0e\xd63\x1a\x08\x02^\xcd\xa1\xc2\xe8m\x171\xbf'
app.config['SESSION_TYPE'] = 'filesystem'
app.config.from_object(__name__)
# Session(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.session_protection = 'strong'
login_manager.login_view = 'signin_from_signout'
login_manager.login_message = 'Please sign in.'
data = None
if 'config.json' in os.listdir():
  try:
    with open('config.json', 'r') as file:
      data = json.load(file)
  except FileNotFoundError as err:
    data = {'db_uri': 'sqlite:///test_alpha.db'}  # Hardcoded fallback
users_db = database_old.UsersDatabase(uri=data['db_uri'])
users_db.setup()
courses_db = database_old.CoursesDatabase(uri=data['db_uri'])
courses_db.setup()
# users_db.admin.set_courses_db(courses_db)
print(users_db.list_usernames())
# print(users_db.new_user(username = 'ken', password = 'test', group = 'test'))
print(users_db.list_usernames())


@app.teardown_appcontext
def teardown_appcontext(exception):
  if users_db is not None:
    users_db.close()


@login_manager.user_loader
def user_loader(user_id):
  if users_db.existing_user(user_id):
    return users_db.get_user(user_id)
  else:
    return None


@login_manager.request_loader
def request_loader(request):
  username = request.form.get('username')
  password = request.form.get('password')
  if username not in users_db.list_usernames():
    return None
  else:
    user = users_db.get_user(username)
    users_db.authentication_status[username] = True
    user.is_authenticated = users_db.check_user(username = username, password = password)
  return user


@app.route('/')
def index():
  return index_path('index.html')


@app.route('/teapot')
def teapot():
  return index_path('teapot.html')


@app.route('/static/<path:path>')
def static_path(path):
  return render_template(os.path.join(os.path.abspath('./static'), path))


@app.route('/signup', methods = ['GET', 'POST'])
def signup():
  if request.method == 'GET':
    return render_template('signup/index.html')
  elif request.method == 'POST':
    username = request.form.get('username')
    password = request.form.get('password')
    print(users_db.list_users())
    users_db.new_user(username = username, password = password)
    return redirect(url_for('signin'))

@app.route('/courses/<course_uuid>/<level>')
@login_required
def instructions(course_uuid, level):
  if courses_db.check_user(current_user.username, course_uuid, int(level)):
    instruc = courses_db.get_course_materials(course_uuid, int(level))
    # print(instruc)
    courses_db.generate_materials(instruc)
    return render_template('courses/course_uuid/index.html', data=instruc)
  else:
    return redirect(url_for('signin'))

@app.route('/signin', methods = ['GET', 'POST'])
def signin():
  if request.method == 'GET':
    return render_template('signin/index.html')
  elif request.method == 'POST':
    username = request.form.get('username')
    password_text = request.form.get('password')
    if password_text is None:
      password_text = ''  # workaround error
    flash(users_db.list_users())
    flash(f'{username}, {password_text}')
    if username not in users_db.list_usernames():
      flash('Username is incorrect.', 'error')
      return render_template('signin/index.html')
    else:
      user = users_db.get_user(username,
                               is_authenticated = users_db.check_user(username = username,
                                                                      password_text = password_text))
      # setattr(user, 'is_authenticated', users_db.check_user(username = username, password_text = password_text))
      if user.is_authenticated:
        login_user(user)
        session['logged_in'] = True
        session.modified = True
        return redirect(url_for('index'))
      else:
        flash('Password is incorrect.', 'error')
        return render_template('signin/index.html')


@app.route('/signin_from_signout')
def signin_from_signout(notifications = None):
  if notifications is None:
    notifications = []
  flash('Sign in required.', 'error')
  return redirect(url_for('signin'))


@app.route('/profile')
@login_required
def profile():
  userInfo = users_db.get_user_profile(current_user.username)
  print("profile")
  course = []
  print(userInfo[2]['courses'])
  for i in userInfo[2]['courses']:
    print(i)
    # print(courses_db.get_course(i['courses'][0][0]))
    course.append(courses_db.get_course(i['courses'][0][0],i['level'][0][0]))
  print(course)
  return render_template('profile/index.html', data=(userInfo[2], course))


@app.route('/profile/<username>')
def publicProfile(username):
  userInfo = users_db.get_user_profile(username)
  print(userInfo)
  course = []
  for i in userInfo[2]['courses']:
    course.append(courses_db.get_course(i['courses'][0][0],i['level'][0][0]))
  print(course)
  return render_template('profile/username/index.html', data=(userInfo[2], course))


@app.route('/settings')
@login_required
def settings():
  return render_template('settings/index.html')


@app.route('/admin')
@login_required
def admin():
  if 'admin' in current_user.group:
    return render_template('admin/index.html', admin = users_db.admin, isinstance = isinstance)
  else:
    return render_template('http_error.html',
                           code = 403,
                           error = f'403 Forbidden: Admin privileges needed. Current groups: {current_user.group}'), 403

def force_halt():
  teardown_appcontext(RuntimeError('Forceful halt by admin from browser.'))
  func = request.environ.get('werkzeug.server.shutdown')
  if func is None:
    raise RuntimeError('Not running with the Werkzeug Server')
  func()


@app.route('/admin/cmd')
@login_required
def admin_cmd():
  if 'admin' in current_user.group:
    confirm = request.args.get('confirm', None)
    cmd = request.args.get('cmd')
    if confirm == 'true':
      if 'force-halt' in cmd:
        force_halt()
        return render_template('http_error.html',
                               code = 200,
                               error = f'200 OK: Forcefully halted.'), 200
      else:
        return render_template('http_error.html',
                               code = 400,
                               error = f'400 Bad Request: Command "{cmd}" not supported.'), 400
    else:
      return render_template('admin/confirm.html', cmd = cmd)
  else:
    return render_template('http_error.html',
                           code = 403,
                           error = f'403 Forbidden: Admin privileges needed. Current groups: {current_user.group}'), 403


@app.route('/courses')
@login_required
def courses():
  print("courses link")
  user_info = users_db.get_user_profile(current_user.username)
  courses_ = user_info[2]['courses']
  res = {}
  for i in courses_:
    print(i)
    c =  courses_db.get_course(i['courses'][0][0], i['level'][0][0])
    if i['courses'][0][0] not in res:
      res[i['courses'][0][0]]={}
      res[i['courses'][0][0]]['name'] = c['overview']['name']
      res[i['courses'][0][0]]['desc'] = c['overview']['desc']
      res[i['courses'][0][0]]['notes'] = c['overview']['notes']
      res[i['courses'][0][0]]['lvl'] = []
      res[i['courses'][0][0]]['lvl'].append(c['overview']['level'])
    else:
      res[i['courses'][0][0]]['lvl'].append(c['overview']['level'])
  print(res)
  return render_template('courses/index.html', data=res)


@app.route('/signout')
@login_required
def signout():
  logout_user()
  return index_path('http_error.html')


@app.route('/<path:path>')
def index_path(path, notifications = []):
  try:
    return render_template(path, notifications = notifications)
  except jinja2.exceptions.TemplateNotFound as err:
    abort(404)


def make_error_handler(code):
  def error_handler(e):
    return render_template(f'errors/http_error.html', error = e, code = code), code


codes = [400, 401, 403, 404, 410, 500, 501, 418]

for code in codes:
  app.register_error_handler(code, make_error_handler(code))

if __name__ == '__main__':
  app.run(host = '0.0.0.0', port = 8080)
