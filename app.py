import json
import os
import traceback
import urllib.parse
import uuid

import strictyaml
from flask import Flask, render_template, redirect, url_for, flash, request, abort
from flask_admin import Admin, AdminIndexView, expose
from flask_admin.contrib.fileadmin import FileAdmin
from flask_admin.contrib.sqla import ModelView
from flask_admin.menu import MenuLink
from flask_login import LoginManager, login_required, current_user, logout_user, login_user
from werkzeug.datastructures import MultiDict

import database
import forms
from admin_views import UserView, CourseView, ActionView


def log_exit(code: int):
  print(f'Exiting with code {code}.')
  exit(code)


print('Reading config from `config.yml`...')
try:
  with open('config.yml', 'r') as file:
    config = strictyaml.load(file.read()).data
except strictyaml.YAMLError as error:
  print(f'`config.yml` has invalid YAML (YAML error). {traceback.format_exc()}')
  log_exit(11)
except strictyaml.YAMLValidationError as error:
  print(f'`config.yml` has invalid YAML (validation). {traceback.format_exc()}')
  log_exit(12)
except strictyaml.StrictYAMLError as error:
  print(f'`config.yml` has invalid YAML (strict YAML). {traceback.format_exc()}')
  log_exit(13)
except FileNotFoundError as error:
  print(f'`config.yml` was not found. {traceback.format_exc()}')
  log_exit(14)
print('Read config from `config.yml`.')

app_name = 'RoboEDU Website'
app = Flask(app_name)
app.config['SECRET_KEY'] = b'\xc3\x0e\xd63\x1a\x08\x02^\xcd\xa1\xc2\xe8m\x171\xbf'
app.config['SESSION_TYPE'] = 'filesystem'
print('Applying flask.config from `config.yml`...')
for key, value in config['flask']['config'].items():
  app.config[key] = value
print('Applied flask.config from `config.yml`.')
app.config.from_object(__name__)
db = database.init_app(app, db_uri = config.get('db_uri', None))
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.session_protection = 'strong'
login_manager.login_view = 'signin'
login_manager.login_message = 'Sign in to continue.'
login_manager.login_message_category = 'warning'
admin = Admin(
  app,
  name = f'{app_name} Admin',
  url = '/admin',
  template_mode = 'bootstrap3')

path = os.path.abspath(os.path.dirname(__file__))
admin.add_view(FileAdmin(path, '/', name = 'DANGER! All Files', category = 'Files'))
admin.add_view(UserView(database.User, db.session, category = 'Users'))
admin.add_view(ModelView(database.CourseUserInstance, db.session, category = 'Users'))
admin.add_view(CourseView(database.Course, db.session, category = 'Courses'))
admin.add_view(ModelView(database.CourseMaterial, db.session, category = 'Courses'))
admin.add_view(ActionView(database.Action, db.session, category = 'Actions'))
admin.add_link(MenuLink(name = 'Non-admin Home', url = '/'))
admin.add_link(
  MenuLink(name = 'Add/Remove/Edit User', url = '/admin/users/index?prev=%2Fadmin%2Fuser', category = 'Users'))
admin.add_link(
  MenuLink(name = 'Add/Remove/Edit Course', url = '/admin/courses/index?prev=%2Fadmin%2Fcourse', category = 'Courses'))


@app.teardown_appcontext
def teardown_appcontext(exception):
  db.session.commit()
  db.session.close()


@login_manager.user_loader
def user_loader(user_id):
  return database.User.query.filter_by(username = user_id).first()


@login_manager.request_loader
def request_loader(request):
  username = request.form.get('username')
  user = database.User.query.filter_by(username = username).first()
  if user:
    user.authenticated = True
  else:
    user = None
  return user


def log_action(user_uid, action_name, action_status, action_desc):
  if user_uid is None:
    user_uid = config['features']['logging']['blank_uuid']
  new_action = database.Action(
    action_uid = str(uuid.uuid4()),
    user_uid = user_uid,
    action_name = action_name,
    action_status = action_status,
    action_desc = action_desc,
  )
  db.session.add(new_action)
  db.session.commit()
  return new_action.action_uid


@app.route("/signin", methods = ["GET", "POST"])
def signin():
  '''
  /signin endpoint.
  For GET requests, display the signin form.
  For POST requests, login the current user by processing the form.
  '''
  form = forms.SigninForm()
  if form.validate_on_submit():
    user = database.User.query.filter_by(username = form.username.data).first()
    if user:
      if user.check_password(form.password.data):
        # if bcrypt.check_password_hash(user.hash, form.password.config):
        user.authenticated = True
        db.session.add(user)
        db.session.commit()
        login_user(user, remember = True)
        log_action(user.user_uid, 'account/signin', '200', 'User signed in successfully.')
        flash('Signed in.', 'ok')
        return redirect(url_for('index'))
      else:
        log_action(user.user_uid, 'account/signin', '4031', 'Password incorrect.')
        flash('Password incorrect.', 'error')
    else:
      log_action(None, 'account/signin', '4032', 'Username not found.')
      flash('Username not found.', 'error')
      flash(database.User.query.all())
  return render_template("accounts/signin.html", form = form)


# @app.route("/signup", methods = ["GET", "POST"])
# def signup():
#   '''
#   /signup endpoint.
#   For GET requests, display the signin form.
#   For POST requests, login the current user by processing the form.
#   '''
#   if config['features']['signup'].lower() == 'false':
#     return render_template('others/http_error.html',
#                            code = 409,
#                            error = f'409 Conflict: Signup is disabled. Enable signup to continue.'), 409
#   elif config['features']['signup'].lower() == 'true':
#     form = forms.SignupForm()
#     if form.validate_on_submit():
#       new_user = database.User(
#         username = form.username.data,
#         user_uid = str(uuid.uuid4()),
#         hash = database.gen_password(
#           form.password.data,
#           config['security']['hashing']['method'],
#           config['security']['hashing']['salt_length']),
#         group = form.group.data,
#         first_name = form.first_name.data,
#         last_name = form.last_name.data,
#         age = form.age.data,
#         notes = form.notes.data,
#         courses = json.dumps([]),
#         authenticated = False
#       )
#       db.session.add(new_user)
#       db.session.commit()
#       # user = database.User.query.get(form.username.data)
#       # if user:
#       #   if user.check_password(form.password.data):
#       #     user.authenticated = True
#       #     db.session.add(user)
#       #     db.session.commit()
#       #     login_user(user, remember = True)
#       #     return redirect(url_for("bull.reports"))
#       #   else:
#       #     flash('Password incorrect.')
#       # else:
#       #   flash('Username not found.')
#     return render_template("accounts/signup.html", form = form)
#   else:
#     return render_template('others/http_error.html',
#                            code = 500,
#                            error = f'500 Internal Server Error: Unknown is Signup is enabled or disabled.'), 500


@app.route("/signout", methods = ["GET"])
@login_required
def signout():
  """Signout the current user."""
  user = current_user
  user.authenticated = False
  db.session.add(user)
  db.session.commit()
  logout_user()
  log_action(user.user_uid, 'account/signout', '200', 'Signed out.')
  flash('Signed out.')
  return redirect(url_for('index'))


@app.route('/')
@login_required
def index():
  if current_user is None:
    user_uid = None
  else:
    user_uid = current_user.user_uid
  log_action(user_uid, 'page/index', '200', 'Index page accessed.')
  return render_template('index.html')


@app.route('/admin/users/<action>', methods = ['GET', 'POST'])
@login_required
def admin_users_action(action: str):
  if 'admin' not in current_user.group:
    return render_template('others/http_error.html',
                           code = 403,
                           error = f'403 Forbidden: Must be admin.'), 403
  prev_url = request.args.get('prev', '/admin')
  if action == 'new':
    form = forms.AdminUserNewForm()
    if form.validate_on_submit():
      try:
        new_user = database.User(
          username = form.username.data,
          user_uid = str(uuid.uuid4()),
          hash = database.gen_password(
            form.password.data,
            config['security']['hashing']['method'],
            config['security']['hashing']['salt_length']),
          password = '',
          group = form.group.data,
          first_name = form.first_name.data,
          last_name = form.last_name.data,
          age = form.age.data,
          notes = form.notes.data,
          courses = json.dumps([]),
          authenticated = False
        )
        db.session.add(new_user)
        db.session.commit()
      except Exception as err:
        log_action(current_user.user_uid, f'admin/users/add', '500', f'Error "{str(err)}" occurred.')
        flash(traceback.format_exc(), 'error')
      log_action(current_user.user_uid, f'admin/users/add', '200', 'User added.')
      flash(f'User '
            f'{form.first_name.data} {form.last_name.data} / {form.username.data} '
            f'({form.group.data}, {new_user.user_uid}) added.', 'ok')
    return render_template('admin/courses/new.html', form = form, prev_url = prev_url)
  elif action == 'edit':
    user_uid = request.args.get('user_uid', None)
    if user_uid is None:
      log_action(current_user.user_uid, f'admin/users/edit', '400', 'User UID not given.')
      flash('User UID not given. Choose the user that you want to edit, and then repeat.', 'error')
    else:
      user = database.User.query.filter_by(user_uid = user_uid).first()
      form = forms.AdminUserEditForm(formdata = MultiDict({
        'username': user.username,
        'password': '',
        'group': user.group,
        'first_name': user.first_name,
        'last_name': user.last_name,
        'age': user.age,
        'notes': user.notes,
      }))
      if form.validate_on_submit():
        try:
          user.username = form.username.data
          user.hash = database.gen_password(
            form.password.data,
            config['security']['hashing']['method'],
            config['security']['hashing']['salt_length'])
          user.group = form.group.data
          user.first_name = form.first_name.data
          user.last_name = form.last_name.data
          user.age = form.notes.data
          db.session.commit()
        except Exception as err:
          flash(traceback.format_exc(), 'error')
        flash(f'User {user.friendlyrepr} edited.', 'ok')
        log_action(current_user.user_uid, f'admin/users/edit', '400', 'User edited.')
      return render_template('admin/users/edit.html', form = form, prev_url = prev_url, user_uid = user_uid)
    return redirect('/admin/users/choose')
  elif action == 'choose':
    form = forms.AdminUserChooseForm()
    choices = []
    for user in database.User.query.all():
      choices.append((user.user_uid, user.friendlyrepr))
    form.user_uid.choices = choices
    if form.validate_on_submit():
      try:
        user_exists = db.session.query(database.User).filter_by(user_uid = form.user_uid.data).scalar() is not None
        if not user_exists:
          flash(f'User "{form.user_uid.data}" doesn\'t exist. This shouldn\' happen!', 'error')
        elif user_exists:
          flash(f'User "{form.user_uid.data}" does exist.', 'debug')
          chosen_user = database.User.query.filter_by(user_uid = form.user_uid.data).first()
          flash(f'User {chosen_user.friendlyrepr} chosen.', 'ok')
          return render_template('admin/users/chosen.html', prev_url = prev_url, user = chosen_user)
      except Exception as err:
        flash(traceback.format_exc(), 'error')
    return render_template('admin/users/choose.html', form = form, prev_url = prev_url)
  elif action == 'index':
    return render_template('admin/users/index.html', prev_url = prev_url)
  else:
    return render_template('others/http_error.html',
                           code = 404,
                           error = f'404 Not Found: Admin User action "{action}" does not exist.'), 404


@app.route('/admin/actions')
@login_required
def admin_action():
  if 'admin' not in current_user.group:
    return render_template('others/http_error.html',
                           code = 403,
                           error = f'403 Forbidden: Must be admin.'), 403
  action_uid = request.args.get('action_uid', None)
  if action_uid is None:
    flash('Action UID not given.', 'error')
  action = database.Action.query.filter_by(action_uid = action_uid).first()
  if action is None:
    return render_template('others/http_error.html',
                           code = 404,
                           error = f'404 Not Found: Action with action UID "{action_uid}" not found.'), 404
  else:
    user = database.User.query.filter_by(user_uid = action.user_uid).first()
  action_status_icon = 'help'
  action_status_type = str(action.action_status)[0]
  if action_status_type == '1':
    action_status_icon = 'info'
  elif action_status_type == '2':
    action_status_icon = 'check'
  elif action_status_type == '3':
    action_status_icon = 'swap_calls'
  elif action_status_type == '4':
    action_status_icon = 'phonelink_erase'
  elif action_status_type == '5':
    action_status_icon = 'error'
  return render_template(
    'admin/actions/index.html',
    action = action,
    user = user,
    action_status_icon = action_status_icon)


@app.route('/admin/courses/<action>', methods = ['GET', 'POST'])
@login_required
def admin_courses_action(action: str):
  if 'admin' not in current_user.group:
    return render_template('others/http_error.html',
                           code = 403,
                           error = f'403 Forbidden: Must be admin.'), 403
  prev_url = request.args.get('prev', '/admin')
  if action == 'new':
    form = forms.AdminCourseNewForm()
    if form.validate_on_submit():
      try:
        new_course = database.Course(
          name = form.name.data,
          desc = form.desc.data,
          notes = form.notes.data,
          course_uid = str(uuid.uuid4()),
          instances = json.dumps([]),
          materials = json.dumps([]),
        )
        db.session.add(new_course)
        db.session.commit()
      except Exception as err:
        flash(traceback.format_exc(), 'error')
      flash(f'Course {new_course.name} ({new_course.course_uid}) added.', 'ok')
    return render_template('admin/courses/new.html', form = form, prev_url = prev_url)
  elif action == 'edit':
    user_uid = request.args.get('user_uid', None)
    if user_uid is None:
      flash('User UID not given. Choose the user that you want to edit, and then repeat.', 'error')
    else:
      user = database.User.query.filter_by(user_uid = user_uid).first()
      form = forms.AdminUserEditForm(formdata = MultiDict({
        'username': user.username,
        'password': '',
        'group': user.group,
        'first_name': user.first_name,
        'last_name': user.last_name,
        'age': user.age,
        'notes': user.notes,
      }))
      if form.validate_on_submit():
        try:
          user.username = form.username.data
          user.hash = database.gen_password(
            form.password.data,
            config['security']['hashing']['method'],
            config['security']['hashing']['salt_length'])
          user.group = form.group.data
          user.first_name = form.first_name.data
          user.last_name = form.last_name.data
          user.age = form.notes.data
          db.session.commit()
        except Exception as err:
          flash(traceback.format_exc(), 'error')
        flash(f'User {user.friendlyrepr} edited.', 'ok')
      return render_template('admin/courses/edit.html', form = form, prev_url = prev_url, course_uid = course_uid)
    return redirect('/admin/courses/choose')
  elif action == 'choose':
    form = forms.AdminUserChooseForm()
    choices = []
    for user in database.User.query.all():
      choices.append((user.user_uid, user.friendlyrepr))
    form.user_uid.choices = choices
    if form.validate_on_submit():
      try:
        user_exists = db.session.query(database.User).filter_by(user_uid = form.user_uid.data).scalar() is not None
        if not user_exists:
          flash(f'User "{form.user_uid.data}" doesn\'t exist. This shouldn\' happen!', 'error')
        elif user_exists:
          flash(f'User "{form.user_uid.data}" does exist.', 'debug')
          chosen_user = database.User.query.filter_by(user_uid = form.user_uid.data).first()
          flash(f'User {chosen_user.friendlyrepr} chosen.', 'ok')
          return render_template('admin/courses/chosen.html', prev_url = prev_url, user = chosen_user)
      except Exception as err:
        flash(traceback.format_exc(), 'error')
    return render_template('admin/courses/choose.html', form = form, prev_url = prev_url)
  elif action == 'index':
    return render_template('admin/courses/index.html', prev_url = prev_url)
  else:
    return render_template('others/http_error.html',
                           code = 404,
                           error = f'404 Not Found: Admin User action "{action}" does not exist.'), 404


@app.route('/profile')
@app.route('/profiles')
@login_required
def profile_uid():
  user_uid = request.args.get('user_uid', None)
  username = request.args.get('username', None)
  if user_uid is not None:
    log_action(current_user.user_uid, f'page/profile', '200', f'Profile "{user_uid}" (user_uid) accessed.')
    return render_template('profile/index.html', len = len,
                           users = [database.User.query.filter_by(user_uid = user_uid).first()])
  elif username is not None:
    log_action(current_user.user_uid, f'page/profile', '200', f'Profile "{username}" (username) accessed.')
    return render_template('profile/index.html', len = len,
                           users = [database.User.query.filter_by(username = username).first()])
  else:
    log_action(current_user.user_uid, f'page/profile', '200', 'All profiles accessed.')
    return render_template('profile/index.html', len = len, users = database.User.query.all())


@app.route('/courses')
@login_required
def course_uid():
  # course_uid = db.Column(db.String(36), primary_key = True, nullable = False)
  # name = db.Column(db.String(1024), nullable = False)
  # desc = db.Column(db.String(2000000000), nullable = False)
  # notes = db.Column(db.String(2000000000), nullable = False)
  # instances = db.Column(db.String(2000000000), nullable = False)  # json of
  # materials = db.Column(db.String(2000000000), nullable = False)  # json of [course_uid, level, i]
  course_uid_ = request.args.get('course_uid', None)
  if course_uid_ is not None:
    raw_courses = [database.Course.query.filter_by(course_uid = course_uid_).first()]
  else:
    raw_courses = database.Course.query.all()
  courses = []
  for raw_course in raw_courses:
    course_uid_ = raw_course.course_uid
    user_uid = current_user.user_uid
    if database.CourseUserInstance.query.filter_by(course_uid = course_uid_, user_uid = user_uid).first() is not None:
      courses.append(raw_course)
  log_action(current_user.user_uid, f'page/courses', '200', 'Courses for user accessed.')
  return render_template('courses/index.html', len = len, courses = courses)


@app.route('/courses/open')
@login_required
def course_open():
  course_uid = request.args.get('course_uid', None)
  if course_uid is None:
    log_action(current_user.user_uid, f'page/courses/open', '400', 'Course UID not given.')
    return render_template('others/http_error.html',
                           code = 400,
                           error = f'400 Bad Request: Course UID not given.'), 400
  else:
    course = database.Course.query.filter_by(course_uid = course_uid).first()
    instance = database.CourseUserInstance.query.filter_by(course_uid = course_uid,
                                                           user_uid = current_user.user_uid).first()
    materials = database.CourseMaterial.query.filter_by(course_uid = course_uid).all()
    if instance is None:
      log_action(current_user.user_uid, f'page/courses/open', '403', f'User not taking course "{course.course_uid}".')
      return render_template('others/http_error.html',
                             code = 403,
                             error = f'403 Forbidden: User "{current_user.user_uid}" is not taking course "{course.course_uid}".'), 403
    levels = []
    for material in materials:
      if material.level not in levels:
        levels.append(material.level)
    status = json.loads(instance.status)
    level = status.get('level', 0)
    log_action(current_user.user_uid, f'page/courses/open', '200', f'Course "{course.course_uid}" opened.')
    return render_template(
      'courses/open/index.html',
      len = len,
      course = course,
      course_uid = course_uid,
      level = level,
      levels = levels, )


@app.route('/courses/start')
@login_required
def course_start():
  course_uid = request.args.get('course_uid', None)
  level = request.args.get('level', None)
  instance = database.CourseUserInstance.query.filter_by(course_uid = course_uid,
                                                         user_uid = current_user.user_uid).first()
  if instance is None:
    log_action(current_user.user_uid, f'page/courses/start', '403', f'User not taking course "{course_uid}".')
    return render_template('others/http_error.html',
                           code = 403,
                           error = f'403 Forbidden: User "{current_user.user_uid}" is not taking course "{course_uid}".'), 403
  if course_uid is None or level is None:
    log_action(current_user.user_uid, f'page/courses/open', '400', 'Course UID not given.')
    return render_template('others/http_error.html',
                           code = 400,
                           error = '400 Bad Request: Course UID or Level not given.'), 400
  else:
    course = database.Course.query.filter_by(course_uid = course_uid).first()
    materials = database.CourseMaterial.query.filter_by(course_uid = course_uid, level = level).all()
    log_action(current_user.user_uid, f'page/courses/start', '200', f'Course "{course.course_uid}" started.')
    return render_template(
      'courses/start/index.html',
      len = len,
      course = course,
      materials = materials,
      range_len_materials = range(len(materials)),
      course_uid = course_uid,
      level = level)


def make_error_handler(code):
  def error_handler(e):
    return render_template(f'errors/http_error.html', error = e, code = code), code


codes = [400, 401, 403, 404, 410, 500, 501]

for code in codes:
  app.register_error_handler(code, make_error_handler(code))
#
if __name__ == '__main__':
  app.run(host = '0.0.0.0', port = 8080)
