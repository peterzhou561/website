import bcrypt
import flask
import werkzeug
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def gen_password(password: str, method: str, salt_length: int) -> str:
  """
  Generates new password for this User instance.
  """
  return werkzeug.security.generate_password_hash(password, method = method, salt_length = int(salt_length))


class Action(db.Model):
  action_uid = db.Column(db.String(36), primary_key = True, nullable = False)
  user_uid = db.Column(db.String(36))
  action_name = db.Column(db.String(2000000000), nullable = False)
  action_status = db.Column(db.String(2000000000), nullable = False)
  action_desc = db.Column(db.String(2000000000), nullable = False)
  
  def __repr__(self):
    return f'<Action {self.action_uid}>'

class User(db.Model):
  username = db.Column(db.String(128), unique = True)
  user_uid = db.Column(db.String(36), primary_key = True, nullable = False)
  hash = db.Column(db.String(20000))
  password = db.Column(db.String(20000), nullable = False)
  group = db.Column(db.String(1024), nullable = False)
  first_name = db.Column(db.String(1024), nullable = False)
  last_name = db.Column(db.String(1024), nullable = False)
  age = db.Column(db.Integer, nullable = False)
  notes = db.Column(db.String(2000000000), nullable = False)
  courses = db.Column(db.String(2000000000), nullable = False)  # json of instance_uid
  authenticated = db.Column(db.Boolean, default = False)
  
  def __repr__(self):
    return f'<User {self.user_uid}-{self.username}>'
  
  @property
  def is_active(self) -> bool:
    """True, as all users are active."""
    return True
  
  
  def get_id(self) -> str:
    """Return the email address to satisfy Flask-Login's requirements."""
    return self.username
  
  @property
  def is_authenticated(self) -> bool:
    """Return True if the user is authenticated."""
    return self.authenticated
  
  def check_password(self, password: str) -> bool:
    """
    Return true if password is correct, false if not.
    """
    print(self.hash, password)
    return werkzeug.security.check_password_hash(self.hash, password)
  
  def gen_password(self, method: str, salt_length: int) -> None:
    """
    Generates new password for this User instance.
    """
    self.hash = werkzeug.security.generate_password_hash(self.password, method = method, salt_length = salt_length)
    db.session.add(self)
    db.session.commit()
  
  @property
  def friendlyrepr(self) -> str:
    return f'{self.first_name} {self.last_name} / {self.username} ({self.group}, {self.user_uid})'
  
  @property
  def shortrepr(self) -> str:
    return f'{self.first_name} {self.last_name} / {self.username}'
  
  @property
  def is_anonymous(self) -> bool:
    """False, as anonymous users aren't supported."""
    return False


class CourseUserInstance(db.Model):
  course_uid = db.Column(db.String(36), nullable = False)
  instance_uid = db.Column(db.String(36), primary_key = True, nullable = False)
  user_uid = db.Column(db.String(36), nullable = False)
  status = db.Column(db.String(1024), nullable = False)
  marks = db.Column(db.String(2000000000), nullable = False)
  time_start = db.Column(db.String(2000000000), nullable = False)
  time_end = db.Column(db.String(2000000000), nullable = False)
  instructor_uid = db.Column(db.String(2000000000), nullable = False)
  notes = db.Column(db.String(2000000000), nullable = False)
  
  def __repr__(self):
    return f'<CourseUserInstance {self.uid}-{self.username}>'


class Course(db.Model):
  course_uid = db.Column(db.String(36), primary_key = True, nullable = False)
  name = db.Column(db.String(1024), nullable = False)
  desc = db.Column(db.String(2000000000), nullable = False)
  notes = db.Column(db.String(2000000000), nullable = False)
  instances = db.Column(db.String(2000000000), nullable = False)  # json of
  materials = db.Column(db.String(2000000000), nullable = False)  # json of [course_uid, level, i]

  @property
  def friendlyrepr(self) -> str:
    return f'{self.name} ({self.course_uid})'

  @property
  def shortrepr(self) -> str:
    return f'{self.name}'

  def __repr__(self):
    return f'<Course {self.uid}-{self.username}>'


class CourseMaterial(db.Model):
  course_uid = db.Column(db.String(36), primary_key = True, nullable = False)
  level = db.Column(db.Integer, primary_key = True, nullable = False)
  i = db.Column(db.Integer, primary_key = True, nullable = False)
  j = db.Column(db.Integer, primary_key = True, nullable = False)
  data_type = db.Column(db.String(1024), nullable = False)
  data = db.Column(db.String(2000000000), nullable = False)
  
  def __repr__(self):
    return f'<CourseMaterial {self.course_uid}>'


def init_app(app: flask.Flask, db_uri: str) -> SQLAlchemy:
  app.config['SQLALCHEMY_DATABASE_URI'] = db_uri
  db.init_app(app)
  db.create_all(app = app)
  return db
